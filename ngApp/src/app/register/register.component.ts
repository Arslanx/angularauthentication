import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
  registerUserdata = {};
  public token = "";
  constructor(private auth: AuthService, public router: Router) {}

  ngOnInit() {}

  registerUser() {
    console.log(this.registerUserdata);
    this.auth.registerUser(this.registerUserdata).subscribe((data) => {
      console.log(data);
      this.token = data["token"];
      localStorage.setItem("token", this.token);
      this.router.navigate(["/user-info"]);

      (err) => console.log(err);
    });
  }
}
