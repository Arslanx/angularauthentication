import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-personal-info",
  templateUrl: "./personal-info.component.html",
  styleUrls: ["./personal-info.component.scss"],
})
export class PersonalInfoComponent implements OnInit {
  registerUserdata = {};
  public imageSrc = null;
  constructor() {}

  ngOnInit() {}

  readURL(event): void {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = (e) => (this.imageSrc = reader.result);

      reader.readAsDataURL(file);
    }
  }

  SaveUser() {
    this.registerUserdata["img"] = this.imageSrc;
    console.log(this.registerUserdata);
  }
}
