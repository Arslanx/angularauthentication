import { Component, OnInit } from "@angular/core";
import { AuthService } from "src/app/auth.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  loginUserdata = {};
  public token = "";
  constructor(private auth: AuthService, private router: Router) {}

  ngOnInit() {}

  loginUser() {
    this.auth.loginUser(this.loginUserdata).subscribe(
      (res) => {
        console.log(res);
        this.token = res["token"];
        localStorage.setItem("token", this.token);
        this.router.navigate(["/special"]);
      },
      (err) => console.log(err)
    );
  }
}
