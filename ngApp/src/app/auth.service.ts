import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  // private _registerUrl = ;
  constructor(private http: HttpClient, public router: Router) {}

  registerUser(params) {
    var url = "http://localhost:3000/api/register";
    var response = this.http.post(url, params, {});
    // .pipe(map((res) => res.json()));
    return response;
  }

  loginUser(params) {
    var url = "http://localhost:3000/api/login";
    var response = this.http.post(url, params, {});
    // .pipe(map((res) => res.json()));
    return response;
  }

  loggedIn() {
    return !!localStorage.getItem("token");
  }

  logout() {
    localStorage.removeItem("token");
    this.router.navigate(["/events"]);
  }
}
