import { Component, OnInit } from "@angular/core";
import { EventService } from "src/app/event.service";
import { HttpErrorResponse } from "@angular/common/http";
import { Router } from "@angular/router";

@Component({
  selector: "app-special-events",
  templateUrl: "./special-events.component.html",
  styleUrls: ["./special-events.component.scss"],
})
export class SpecialEventsComponent implements OnInit {
  public specialEvents = [];
  constructor(public _eventService: EventService, public router: Router) {}

  ngOnInit() {
    this._eventService.getspecialEvents().subscribe(
      (res) => (this.specialEvents = res),
      (err) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401) {
            this.router.navigate(["/login"]);
          }
        }
      }
    );
  }
}
