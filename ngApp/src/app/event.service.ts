import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class EventService {
  constructor(private http: HttpClient) {}

  getEvents(): Observable<any> {
    var url = "http://localhost:3000/api/events";
    var response = this.http.get(url, {});
    return response;
  }

  getspecialEvents(): Observable<any> {
    var url = "http://localhost:3000/api/special";
    var response = this.http.get(url, {});
    return response;
  }
}
